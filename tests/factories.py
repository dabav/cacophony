"""Various convenience model factories.
"""

import factory
import factory.fuzzy
from cacophony import models


class VoteFactory(factory.Factory):
    class Meta:
        model = models.Vote

    direction = factory.fuzzy.FuzzyChoice([-1, 1])
    magnitude = factory.fuzzy.FuzzyInteger(1, 10)
