"""Make sure that the testing environment is sane.
"""

from cacophony.models import User


def test_database_env1():
    """Part one of database sanity testing.
    """
    user = User(email="foobar", password="aa")
    user.save()

    assert User.query.count() == 1
    assert User.query.all()[0].email == "foobar"


def test_database_env2():
    """Counterpart to _env1.
    """
    user = User(email="barbaz", password="aa")
    user.save()

    assert User.query.count() == 1
    assert User.query.all()[0].email == "barbaz"
