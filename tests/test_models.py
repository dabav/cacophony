from tests import factories
import factory
from cacophony import models


def test_point_add_vote():
    """Make sure that adding votes works correctly.
    """
    point = models.Point()
    votes = factory.helpers.create_batch(
        factories.VoteFactory,
        500)

    total_magnitude = 0
    total_ratings = 0
    total_points = 0

    for vote in votes:
        point.add_vote(vote)
        total_points += vote.direction
        if vote.direction == 1:
            total_ratings += 1
            total_magnitude += vote.magnitude

    assert point.num_ratings == total_ratings
    assert point.points == total_points
    assert abs(point.magnitude - total_magnitude / total_ratings) < .01
