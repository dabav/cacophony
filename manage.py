#!/usr/bin/env python
"""Script for managing Cacophony - database, running, etc.
"""
import sys
import subprocess
from cacophony.cacophony import create_app
from cacophony.cacophony import db
from flask import current_app
from flask.cli import FlaskGroup
from sqlalchemy import create_engine
from sqlalchemy.engine.url import make_url, URL
from sqlalchemy.exc import OperationalError, ProgrammingError
import click
import sqlalchemy_utils


@click.pass_context
def get_app(ctx, _):
    """Create and return an app running the given config.
    """
    return create_app(ctx.find_root().params["config"])


@click.option("-c", "--config", default="development")
@click.group(cls=FlaskGroup, create_app=get_app)
def cli(**_):
    """Set up the cli command group.
    """
    pass


@cli.command("create-db")
def create_db():
    """Bootstrap the database for this config.

    This means:

    1. Attempt to connect to the database. If successful, do nothing.

    2. If 1 is unsusccessful, then request root permissions. Create a user and
    database and grant ownership of the database to the user.
    """
    try:
        db.engine.connect()
    except OperationalError as e:
        click.echo(("Looks like the database is not configured. The error "
                    "from SQLAlchemy is: "))
        click.echo(e)
        click.echo(("If you enter the database credentials, I will try to "
                    "set up the database. If any conflicts exist, nothing "
                    "will happen - this operation is safe."))
        root_name = click.prompt("Enter root username", type=str)
        root_pass = click.prompt("Enter root password", type=str,
                                 hide_input=True)
        db_uri = make_url(current_app.config["SQLALCHEMY_DATABASE_URI"])
        root_uri = URL(db_uri.drivername,
                       root_name,
                       root_pass,
                       db_uri.host,
                       db_uri.port,
                       db_uri.database)

        try:
            sqlalchemy_utils.functions.create_database(root_uri)
        except ProgrammingError as e:
            click.echo("Got error while creating database")
            click.echo(e)

        engine = create_engine(root_uri)
        conn = engine.connect()
        conn.execute("commit")
        try:
            conn.execute(("CREATE USER {} WITH PASSWORD '{}';").
                         format(db_uri.username, db_uri.password))
        except ProgrammingError as e:
            click.echo("Got error while creating user:")
            click.echo(e)
        conn.execute(("ALTER DATABASE {} OWNER TO {};").format(
            db_uri.database, db_uri.username))
        conn.close()

        return
    click.echo("Database seems to be working OK.")


@cli.command("test")
def tests():
    """Set the app config to testing and run pytest, passing along command
    line args.
    """
    sys.exit(subprocess.call(["py.test",
                              "--flake8",
                              "--cov=cacophony"
                              # "--pylint",
                              ]))


if __name__ == '__main__':
    cli()
