
"""Configurations for the project. These are loaded in app.py.
"""

import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    """Global default config.
    """

    DEBUG = False
    SECRET_KEY = "---"
    SECURITY_PASSWORD_SALT = "---"
    SECURITY_REGISTERABLE = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TESTING = False
    WTF_CSRF_ENABLED = True
    WTF_CSRF_METHODS = ["POST", "PUT", "PATCH", "DELETE"]


class Production(Config):
    """Configuration for production environments.
    """
    DEBUG = False
    TESTING = False
    SECURITY_PASSWORD_HASH = "bcrypt"


class Development(Config):
    """Configuration for development environments.
    """
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "postgresql://cacophony:pass@localhost/cacophony"
    SECURITY_SEND_REGISTER_EMAIL = False
    SQLALCHEMY_ECHO = True
    SECURITY_PASSWORD_HASH = "bcrypt"


class Testing(Config):
    """Config used for testing.
    """
    DEBUG = True
    TESTING = True
    WTF_CSRF_ENABLED = False
    SQLALCHEMY_DATABASE_URI = ("postgresql://cacophony:pass@localhost/"
                               "cacophony_test")


configs = {
    "production": Production,
    "development": Development,
    "testing": Testing,
}
