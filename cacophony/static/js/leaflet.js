var initial_lat = 34.06
var initial_lng = -118.44
var initial_zoom = 14
var mainMap = L.map('mapid').setView([initial_lat, initial_lng], initial_zoom);
var bounds = mainMap.getBounds();
var newMarker;  // Store a potential new marker here

L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGFiYXYiLCJhIjoiY2lyaGR0bDB6MDFicWdibnJoMnhldjhsNiJ9.bFWtbmDdGe9iupal1MqNsQ', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        }).addTo(mainMap);

function bindCreationForm(marker) {
  var form = $("#select-point-form").clone()[0]
  return marker.bindPopup(form);
}

function updateNewMarker(latlng) {
    if(newMarker) {
        mainMap.removeLayer(newMarker);
        newMarker = new L.marker(latlng);
        mainMap.addLayer(newMarker);
    } else {
        newMarker = new L.marker(latlng);
        mainMap.addLayer(newMarker);
    }
    return newMarker;
}

function renderSearchResults(results) {
    bindCreationForm(updateNewMarker([results[0].lat, results[0].lon])).openPopup();
    var bbox = results[0].boundingbox,
        first = new L.LatLng(bbox[0], bbox[2]),
        second = new L.LatLng(bbox[1], bbox[3]),
        bounds = new L.LatLngBounds([first, second]);
    this._map.fitBounds(bounds);
}

var osmGeocoder = new L.Control.OSMGeocoder({
    bounds: bounds,
    callback: renderSearchResults, 
});

function onMapClick(e) {
    bindCreationForm(updateNewMarker(e.latlng)).openPopup();
}

mainMap.addControl(osmGeocoder);
mainMap.on('click', onMapClick);
