"""Handle creating the app and configuring it.
"""
from flask import Flask
from flask_wtf.csrf import CsrfProtect
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore
from flask_migrate import Migrate

from cacophony import config


db = SQLAlchemy()
csrf = CsrfProtect()
security = Security()
migrate = Migrate()


def create_app(config_name="development", overrides=None):
    """Create and return an instance of this application.
    """
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object(config.configs[config_name])
    app.config.from_pyfile("instance_config.py", silent=True)
    if overrides:
        app.config.from_mapping(overrides)

    print("Using config: " + config_name)

    db.init_app(app)
    csrf.init_app(app)
    migrate.init_app(app, db)

    from cacophony.models import User, Role
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    security.init_app(app, user_datastore)
    # Workaround for flask-security bug #383
    security.datastore = user_datastore
    security.app = app

    from cacophony.views.core import core

    app.register_blueprint(core)

    return app
