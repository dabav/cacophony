"""Views for cacophony.
"""

from flask import Blueprint, render_template

core = Blueprint("core", __name__, url_prefix="/")


@core.route("/", methods=["GET"])
def read_map():
    """Display a map with Points.
    """
    return render_template("core/index.html")
