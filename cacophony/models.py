"""Models for the Cacophony project.
"""

from cacophony.cacophony import db
from flask_security import UserMixin, RoleMixin


class Base(db.Model):
    """Base class for all models.
    All models have an identical id field.
    Attributes:
        id (int): A unique identifier.
    """
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)

    def save(self, commit=True):
        """Save this model to the database.
        If commit is True, then the session will be comitted as well.
        """
        db.session.add(self)

        if commit:
            db.session.commit()


roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(),
                                 db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(),
                                 db.ForeignKey('role.id')))


class Role(Base, RoleMixin):
    """A Role describes what a User can and can't do.
    """
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(Base, UserMixin):
    """A User is used for authentication.
    Attributes:
        name (string): The username of this user.
        password (string): This user's password.
        authenticated (bool): True if this user is authenticated.
    """

    email = db.Column(db.String(255), unique=True)
    active = db.Column(db.Boolean())
    password = db.Column(db.String(255), nullable=False)
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship("Role", secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    votes = db.relationship("Vote", back_populates="user")
    created_points = db.relationship("Point", back_populates="creator")


class Point(Base):
    """A Point is a house that has been reported as noisy.

    ``points`` is a convenience attribute to get the number of points this
    Point has, which is the sum of all the ``Vote.direction`` attributes in
    this Point's ``points`` attribute.

    ``ratings`` is a convenience attribute that reflects how many Votes in
    ``votes`` also included a magnitude rating. This way recalculating the
    average magnitude is fast.

    Attributes:
        creator (User): Which user created this
        votes (list of Vote): List of Votes cast on this Point
        points (int): How many points (upvotes) this Point has
        num_ratings (int): Number of magnitude ratings
        magnitude (float): How noisy this place is out of 10
        latitude (float): Latitude of this Point
        longitude (float): Longitude of this Point
    """
    creator_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    creator = db.relationship("User", back_populates="created_points")

    votes = db.relationship("Vote", back_populates="point")

    points = db.Column(db.Integer, default=0)
    num_ratings = db.Column(db.Integer, default=0)
    magnitude = db.Column(db.Float, default=0)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)

    def add_vote(self, vote):
        """Given a Vote object, add it to ``votes`` and update this object
        accordingly.
        """
        self.votes.append(vote)

        self.points += vote.direction

        if vote.magnitude and vote.direction > 0:
            self.num_ratings += 1
            self.magnitude = self.magnitude + \
                (vote.magnitude - self.magnitude) / \
                (self.num_ratings)

    def __init__(self, *args, **kwargs):
        """Initialize some fields to 0, then call the superclass constructor.
        """
        self.points = 0
        self.magnitude = 0
        self.num_ratings = 0

        super().__init__(*args, **kwargs)


class Vote(Base):
    """A Vote is a User judging a Point.

    Votes increase the standing of a Point, and also include a Magnitude, or
    how loud a point is.

    Attributes:
        user (User): The User that cast this Vote
        point (Point): The Point the Vote was casted on
        direction (int): -1 for downvote, 1 for upvote
        magnitude (int): Rating of this Point's loudness, on a 10 pt scale
    """
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = db.relationship("User", back_populates="votes")

    point_id = db.Column(db.Integer, db.ForeignKey("point.id"))
    point = db.relationship("Point", back_populates="votes")

    direction = db.Column(db.Integer)

    magnitude = db.Column(db.Integer)
